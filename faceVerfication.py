import cv2
import sys
#from Image import Image
import logging as log
import datetime as dt
from time import sleep
from playsound import playsound

cascPath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascPath)
log.basicConfig(filename='webcam.log',level=log.INFO)

video_capture = cv2.VideoCapture(0)
anterior = 0
bye = "have a good day"
histogram1=""
histogram2=""
print("press q to take a picture")

while True:

    if not video_capture.isOpened():
        print('Unable to load camera.')
        sleep(2)
        pass
        break

    else:     
      
        # Capture frame-by-frame
        ret, frame, = video_capture.read()
        #playsound()

        

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30)
        )
        # Draw a rectangle around the faces
        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)


        if anterior != len(faces):
            anterior = len(faces)
            log.info("faces: "+str(len(faces))+" at "+str(dt.datetime.now()))



        
        # Display the resulting frame
        cv2.imshow('Video', frame)
       

        if cv2.waitKey(1) & 0xFF == ord('q'):
            #cv2.imwrite("image.jpg",frame)
            
            name = 1

            for (x, y, w, h) in faces:
                cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
                cropped = frame[y: y + h, x: x + w]
                cv2.imwrite("cropped" + str(name) + ".png", cropped)
                name = name + 1

            cropped1_image = cv2.imread("cropped1.png")
            cropped1_image = (cv2.cvtColor(cropped1_image, cv2.COLOR_BGR2GRAY))
            histogram1 = cv2.calcHist([cropped1_image], [0], None, [256], [0, 256])


            cropped2_image = cv2.imread("cropped2.png")
            cropped2_image = (cv2.cvtColor(cropped2_image, cv2.COLOR_BGR2GRAY))
            histogram2 = cv2.calcHist([cropped2_image], [0], None, [256], [0, 256])

            c1, c2 = 0, 0

            # Euclidean Distace between cropped1 and cropped2
            i = 0
            while i<len(histogram1) and i<len(histogram2):
                c1+=(histogram1[i]-histogram2[i])**2
                i+= 1
            c1 = c1**(1 / 2)

            if (c1>=2000):
                print("Images are verfied")
            
            else:
                print("Images are not verified")
            print(bye)
            
            break

            # Display the resulting frame
    cv2.imshow('Video', frame)



# When everything is done, release the capture
video_capture.release()
cv2.destroyAllWindows()


